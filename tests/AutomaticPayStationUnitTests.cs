using Xunit;

namespace CodingGame.AutomaticPayStation.Tests
{
    public class AutomaticPayStationUnitTests
    {
        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        [InlineData(3)]
        public void NoChangePossible_ReturnsNull(long c)
        {
            //Arrange & Act
            var change = AutomaticPayStation.Change(c);

            //Assert
            Assert.Null(change);
        }

        [Theory]
        [InlineData(2)]
        [InlineData(4)]
        [InlineData(6)]
        [InlineData(8)]
        public void SmallEven_ReturnOnlyCoins2(long c)
        {
            //Arrange & Act
            var change = AutomaticPayStation.Change(c);

            //Assert
            Assert.Equal(0, change.Bill10);
            Assert.Equal(0, change.Bill5);
            Assert.NotEqual(0, change.Coin2);
        }

        [Theory]
        [InlineData(5)]
        [InlineData(7)]
        [InlineData(9)]
        [InlineData(11)]
        [InlineData(13)]
        public void SmallUneven_ReturnOne5BillAndNo10Bill(long c)
        {
            //Arrange & Act
            var change = AutomaticPayStation.Change(c);

            //Assert
            Assert.Equal(0, change.Bill10);
            Assert.Equal(1, change.Bill5);
        }


        [Theory]
        [InlineData(10)]
        [InlineData(12)]
        [InlineData(14)]
        [InlineData(15)]
        [InlineData(16)]
        [InlineData(17)]
        [InlineData(18)]
        [InlineData(19)]
        public void Tens_ExceptElevenAndThirteen_ReturnOne10Bill(long c)
        {
            //Arrange & Act
            var change = AutomaticPayStation.Change(c);

            //Assert
            Assert.Equal(1, change.Bill10);
        }

        [Fact]
        public void Twenty_Change()
        {
            //Arrange & Act
            var change = AutomaticPayStation.Change(20);

            //Assert
            Assert.Equal(2, change.Bill10);
        }

        [Fact]
        public void TwentyOne_Change()
        {
            //Arrange & Act
            var change = AutomaticPayStation.Change(21);

            //Assert
            Assert.Equal(1, change.Bill10);
            Assert.Equal(1, change.Bill5);
            Assert.Equal(3, change.Coin2);
        }

        [Fact]
        public void TwentyTwo_Change()
        {
            //Arrange & Act
            var change = AutomaticPayStation.Change(22);

            //Assert
            Assert.Equal(2, change.Bill10);
            Assert.Equal(1, change.Coin2);
        }

        [Fact]
        public void TwentyThree_Change()
        {
            //Arrange & Act
            var change = AutomaticPayStation.Change(23);

            //Assert
            Assert.Equal(1, change.Bill10);
            Assert.Equal(1, change.Bill5);
            Assert.Equal(4, change.Coin2);
        }
    }
}

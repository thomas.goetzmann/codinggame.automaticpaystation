﻿using System;

namespace CodingGame.AutomaticPayStation
{
    public class Change
    {
        public long Bill10 { get; set; } = 0;
        public long Bill5 { get; set; } = 0;
        public long Coin2 { get; set; } = 0;
    }

    public class AutomaticPayStation
    {
        /// <summary>
        /// Returns the change for any given positive amount. 
        /// </summary>
        /// <remarks>
        /// WARNING: This AutomaticPayStation has only 10€ and 5€ bills as well as 2€ coins (the amount for each bill or coin is considered unlimited)
        /// </remarks>
        /// <param name="amount">Amount to change and give back to the customer</param>
        /// <returns>Change or null if impossible to return the amount asked.</returns>
        public static Change Change(long amount)
        {
            //Negative amounts will also return null
            if (amount <= 1 || amount == 3)
                return null;

            var change = new Change
            {
                Bill10 = amount / 10
            };

            //Give 1 less Bill10 when the remaining amount is 1 or 3 which is impossible to get with the available bills and coins.
            var restAfterGivingOnly10s = amount % 10;
            if (restAfterGivingOnly10s == 1 || restAfterGivingOnly10s == 3)
            {
                change.Bill10--;
            }

            var remaining = amount - (10 * change.Bill10);
            //Remaining values can only be 2,4,5,6,7,8,9,11 or 13
            if (remaining % 2 == 0)
            {
                change.Coin2 = remaining / 2;
                return change;
            }
            
            //Remaining values can only be 5,7,9,11 or 13
            change.Bill5 = 1;
            remaining -= 5;

            //Remaining values can only be 2,4,6 or 8
            change.Coin2 = remaining / 2;
            return change;
        }
    }
}
